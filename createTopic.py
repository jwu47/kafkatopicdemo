import json, os, sys
import requests
from git import Repo
import shutil, re
import fileinput
import codecs
from tempfile import mkstemp
from shutil import move
from os import remove

# helper
dbquote = '"'


## lib- one-off set up for Docker kafka cluster
def setup_docker():
    os.environ['DOCKER_HOST']='tcp://192.168.99.100:2376'
    os.environ['DOCKER_MACHINE_NAME']='confluent'
    os.environ['SSH_AUTH_SOCK']='/private/tmp/com.apple.launchd.cek5NQwKdN/Listeners'
    os.environ['DOCKER_TLS_VERIFY']='1'
    os.environ['DOCKER_CERT_PATH']='/Users/JWu/.docker/machine/machines/confluent'
    os.environ['SHLVL']='1'


## lib -  topic is a dict from the Topic json defintion. This uses Docker specific command
## but we can easily change that to use the command from Kafka Console
def create_topic(topic):
    create_cmd = ('docker run --net=host --rm confluentinc/cp-kafka:4.0.0 ' +
                 'kafka-topics --create --topic '+ topic['topic_name'] +
                ' --partitions ' + str(topic['partitions']) + ' --replication-factor ' +
                  str(topic['replication_factor']) + '  --if-not-exists --zookeeper localhost:32181')
    print( "----")
    print( "this is the command running to create the topic")
    print(create_cmd)
    os.system(create_cmd)
    print ("Bingo! Topic " + topic_name + " created! (or exists already!)")
    print( "----")



# Step 0. load config
config = json.load(open('kafkaparse-config.json'))

repo_name = config["repo_name"]
print(repo_name)
branch = config["branch"]
template_uri = config["template_uri"]

# 0.5 your secret, don't check in your password
secret = json.load(open('secret.json'))
username = secret["repo_username"]
password = secret["repo_password"]



# step 1, local dir,
# for now - override local dir
if os.path.exists(repo_name):
   print ("info: local directory refreshed from BitBucket: " +  repo_name )
   shutil.rmtree(repo_name)

os.makedirs(repo_name)



# step 2 -  clone the template into local repo
Repo.clone_from(
    'https://' + username + ':' + password + ":" + template_uri,
    repo_name,
    branch='master'
)

# Step 3 loop thru the files and do damage
dir = os.getcwd()
repodir = dir + '/' + repo_name

#ad-hoc for local docker 3-node Kafka cluster
setup_docker()

for subdir, dirs, files in os.walk(repodir):
    for file in files:
        fpath = os.path.join(subdir, file)
        fname = os.path.basename(fpath)

        if (".json"  in fpath) :
            print(os.path.join(repo_name, fname))
            topic_array = json.load(open(os.path.join(repo_name,fname )))
            for topic in topic_array:
                topic_obj = topic['topic']
                topic_name = topic_obj['topic_name']
                topic_paritions = topic_obj['partitions']
                topic_env =  topic_obj['environment']
                print('Kafka topic to create: ' + topic_name)

                 #enforcing name convetions
                if (topic_name.endswith(topic_env) and ((topic_env == 'prd') or (topic_env =='stg'))):
                    create_topic(topic_obj)
                else:
                    print('ERROR -- topic name suffix needs to match environment: ' + topic_name)
                    print('Quit the process!')

                    sys.exit()

                if (topic_paritions > 1000):
                    print('ERROR -- Wow, you want > 1000 paritions on this topic: ' + topic_name)
                    print('You have to override me after some discussion!')
                    print('Quit the process!')

                    sys.exit()
