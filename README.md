# README #

Demo code for Kafka Topic creation. 

Need - 
a) python2 (not python3)
b) access to GitHub repos where Kafka topic meta data (in json format) are stored
c) access to Kafka console

Helpful links
1) set up 3 node docker based Kafka cluster

2) commands with docker kafka
    
     //list
     > docker run     --net=host     --rm     confluentinc/cp-kafka:4.0.0     kafka-topics --list --zookeeper localhost:32181

     //create
     > docker run --net=host --rm confluentinc/cp-kafka:4.0.0 kafka-topics --create --topic parcel-samp1ley-prd --partitions 2 --replication-factor 3  --if-not-exists --zookeeper localhost:32181

     //describe
     docker run     --net=host     --rm     confluentinc/cp-kafka:4.0.0     kafka-topics --describe --topic parcel-sampley-prd  --zookeeper localhost:32181